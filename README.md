# php-extended/polyfill-php80-str-utils

A php implementation of string functions introduced in php8 and above


![coverage](https://gitlab.com/php-extended/polyfill-php80-str-utils/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/polyfill-php80-str-utils/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/polyfill-php80-str-utils": "^1"`


## Basic Usage

This library gives the following functions :

- `str_contains(?string $haystack, ?string $needle) : bool`
- `str_icontains(?string $haystack, ?string $needle) : bool`
- `str_starts_with(?string $haystack, ?string $needle) : bool`
- `str_istarts_with(?string $haystack, ?string $needle) : bool`
- `str_ends_with(?string $haystack, ?string $needle) : bool`
- `str_iends_with(?string $haystack, ?string $neede) : bool`

Those functions follow the naming convention of `str_ireplace`, 
and the parameter placement of `str_contains`.


## License

MIT (See [license file](LICENSE)).
