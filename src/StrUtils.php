<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-php80-str-utils library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Polyfill;

/**
 * StrUtils class file.
 * 
 * This class encapsulate the string functions that were created on php8.
 * (naming : str_contains, str_starts_with, str_ends_with and new case
 * insensitive variants).
 * 
 * @author Anastaszor
 */
final class StrUtils
{
	
	/**
	 * Determine if a string contains a given substring, case sensitive.
	 * Empty/null strings can contain nothing, and all strings contain the
	 * empty/null substring.
	 * 
	 * @param ?string $haystack
	 * @param ?string $needle
	 * @return boolean
	 */
	public static function strContains(?string $haystack, ?string $needle) : bool
	{
		if(null === $needle || '' === $needle || $needle === $haystack)
		{
			return true;
		}
		
		if(null === $haystack || '' === $haystack)
		{
			return false;
		}
		
		return false !== \mb_strpos((string) $haystack, (string) $needle, 0, '8bit');
	}
	
	/**
	 * Determine if a string contains a given substring, case insensitive.
	 * Empty/null strings can contain nothing, and all strings contain the
	 * empty/null substring.
	 * 
	 * @param ?string $haystack
	 * @param ?string $needle
	 * @return boolean
	 */
	public static function striContains(?string $haystack, ?string $needle) : bool
	{
		if(null === $needle || '' === $needle || $needle === $haystack)
		{
			return true;
		}
		
		if(null === $haystack || '' === $haystack)
		{
			return false;
		}
		
		return false !== \mb_stripos((string) $haystack, (string) $needle, 0, '8bit');
	}
	
	/**
	 * Checks if a string starts with a given substring, case sensitive.
	 * Empty/null strings starts with no substring, and all strings starts with
	 * the empty/null substring.
	 * 
	 * @param ?string $haystack
	 * @param ?string $needle
	 * @return boolean
	 */
	public static function strStartsWith(?string $haystack, ?string $needle) : bool
	{
		if(null === $needle || '' === $needle || $needle === $haystack)
		{
			return true;
		}
		
		if(null === $haystack || '' === $haystack)
		{
			return false;
		}
		
		return 0 === \strncmp((string) $haystack, (string) $needle, \mb_strlen((string) $needle, '8bit'));
	}
	
	/**
	 * Checks if a string starts with a given substring, case insensitive.
	 * Empty/null strings starts with no substring, and all strings starts with
	 * the empty/null substring.
	 * 
	 * @param ?string $haystack
	 * @param ?string $needle
	 * @return boolean
	 */
	public static function striStartsWith(?string $haystack, ?string $needle) : bool
	{
		if(null === $needle || '' === $needle || $needle === $haystack)
		{
			return true;
		}
		
		if(null === $haystack || '' === $haystack)
		{
			return false;
		}
		
		$nhaystack = \mb_substr((string) $haystack, \mb_strlen((string) $needle, '8bit'), null, '8bit');
		
		return 0 === \strcasecmp($nhaystack, (string) $needle);
	}
	
	/**
	 * Checks if a string ends with a given substring, case sensitive.
	 * Empty/null strings ends with no substring, and all strings ends with the
	 * empty/null substring.
	 * 
	 * @param ?string $haystack
	 * @param ?string $needle
	 * @return boolean
	 */
	public static function strEndsWith(?string $haystack, ?string $needle) : bool
	{
		if(null === $needle || '' === $needle || $needle === $haystack)
		{
			return true;
		}
		
		if(null === $haystack || '' === $haystack)
		{
			return false;
		}
		
		$needleLength = \mb_strlen((string) $needle, '8bit');
		
		return \mb_strlen((string) $haystack, '8bit') >= $needleLength
			&& 0 === \substr_compare((string) $haystack, (string) $needle, -$needleLength, $needleLength, false);
	}
	
	/**
	 * Checks if a string ends with a given substring, case insensitive.
	 * Empty/null strings ends with no substring, and all strings ends with the
	 * empty/null substring.
	 * 
	 * @param ?string $haystack
	 * @param ?string $needle
	 * @return boolean
	 */
	public static function striEndsWith(?string $haystack, ?string $needle) : bool
	{
		if(null === $needle || '' === $needle || $needle === $haystack)
		{
			return true;
		}
		
		if(null === $haystack || '' === $haystack)
		{
			return false;
		}
		
		$needleLength = \mb_strlen((string) $needle, '8bit');
		
		return \mb_strlen((string) $haystack, '8bit') >= $needleLength
			&& 0 === \substr_compare((string) $haystack, (string) $needle, -$needleLength, $needleLength, true);
	}
	
}
