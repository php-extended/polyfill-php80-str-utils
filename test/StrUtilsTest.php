<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/polyfill-php80-str-utils library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Polyfill\StrUtils;
use PHPUnit\Framework\TestCase;

/**
 * StrUtilsTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Polyfill\StrUtils
 *
 * @internal
 *
 * @small
 */
class StrUtilsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StrUtils
	 */
	protected $_object;
	
	public function testStrContainsSuccess() : void
	{
		$this->assertTrue($this->_object->strContains('abcabc', 'bca'));
	}
	
	public function testStrContainsFailed() : void
	{
		$this->assertFalse($this->_object->strContains('abcabc', 'BCA'));
	}
	
	public function testStrContainsNull() : void
	{
		$this->assertTrue($this->_object->strContains('abcabc', null));
	}
	
	public function testStrContainsNoNull() : void
	{
		$this->assertFalse($this->_object->strContains(null, 'a'));
	}
	
	public function testStriContainsSuccess() : void
	{
		$this->assertTrue($this->_object->striContains('ABCABC', 'bca'));
	}
	
	public function testStriContainsFailed() : void
	{
		$this->assertFalse($this->_object->striContains('ABCABC', 'D'));
	}
	
	public function testStriContainsNull() : void
	{
		$this->assertTrue($this->_object->striContains('ABCABC', null));
	}
	
	public function testStriContainsNoNull() : void
	{
		$this->assertFalse($this->_object->striContains(null, 'A'));
	}
	
	public function testStrStartsWithSuccess() : void
	{
		$this->assertTrue($this->_object->strStartsWith('abcabc', 'abc'));
	}
	
	public function testStrStartsWithFailed() : void
	{
		$this->assertFalse($this->_object->strStartsWith('abcabc', 'ABC'));
	}
	
	public function testStrStartsWithNull() : void
	{
		$this->assertTrue($this->_object->strStartsWith('abcabc', null));
	}
	
	public function testStrStartsWithNoNull() : void
	{
		$this->assertFalse($this->_object->strStartsWith(null, 'abc'));
	}
	
	public function testStriStartsWithSuccess() : void
	{
		$this->assertTrue($this->_object->striStartsWith('ABCABC', 'abc'));
	}
	
	public function testStriStartsWithFailed() : void
	{
		$this->assertFalse($this->_object->striStartsWith('ABCABC', 'BCA'));
	}
	
	public function testStriStartsWithNull() : void
	{
		$this->assertTrue($this->_object->striStartsWith('abcabc', null));
	}
	
	public function testStriStartsWithNoNull() : void
	{
		$this->assertFalse($this->_object->striStartsWith(null, 'abc'));
	}
	
	public function testStrEndsWithSuccess() : void
	{
		$this->assertTrue($this->_object->strEndsWith('abcabc', 'abc'));
	}
	
	public function testStrEndsWithFailed() : void
	{
		$this->assertFalse($this->_object->strEndsWith('abcabc', 'ABC'));
	}
	
	public function testStrEndsWithNull() : void
	{
		$this->assertTrue($this->_object->strEndsWith('abcabc', null));
	}
	
	public function testStrEndsWithNoNull() : void
	{
		$this->assertFalse($this->_object->strEndsWith(null, 'abc'));
	}
	
	public function testStriEndsWithSuccess() : void
	{
		$this->assertTrue($this->_object->striEndsWith('ABCABC', 'abc'));
	}
	
	public function testStriEndsWithFailed() : void
	{
		$this->assertFalse($this->_object->striEndsWith('ABCABC', 'D'));
	}
	
	public function testStriEndsWithNull() : void
	{
		$this->assertTrue($this->_object->striEndsWith('ABCABC', null));
	}
	
	public function testStriEndsWithNoNull() : void
	{
		$this->assertFalse($this->_object->striEndsWith(null, 'abc'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StrUtils();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
